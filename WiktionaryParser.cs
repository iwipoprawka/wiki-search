﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net;

namespace WikiSearch
{
    class WiktionaryParser
    {
        private HttpClient Client;

        // Initialize HTTP client for MediaWiki I/O
        // Responses are expected to be in JSON format
        public WiktionaryParser(string url)
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri(url);
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // On program exit, dispose of HTTP client
        ~WiktionaryParser()
        {
            Client.Dispose();
        }

        // Parse page to response object
        public WiktionaryResponse Parse(string pageName, int sectionId = 0)
        {
            string url = "/w/api.php?action=parse&format=json&page=" + pageName;

            if (sectionId > 0)
            {
                url += "&section=" + sectionId;
            }

            HttpResponseMessage response = Client.GetAsync(url).Result;

            if (response.IsSuccessStatusCode)
            {
                string stringResponse = response.Content.ReadAsStringAsync().Result;
                WiktionaryResponse wikiResponse = JsonConvert.DeserializeObject<WiktionaryResponse>(stringResponse);
                return wikiResponse;
            }
            else
            {
                return null;
            }
        }

        // Download file through HTTP
        public void DownloadFile(string url, string targetFilePath)
        {
            using (var webClient = new WebClient())
            {
                webClient.DownloadFile(url, targetFilePath);
            }
        }
    }
}
