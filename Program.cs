﻿using System.Threading.Tasks;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace WikiSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            WiktionaryParser parser = new WiktionaryParser("http://en.wiktionary.org/");
            string Language = "Polish";
            string Word = "";
            string IPANotation = "";
            string SoundFileUrl = "";

            //// Main page "Index:English"
            WiktionaryResponse mainPageResponse = parser.Parse("Index:" + Language);

            // Get list of subpages (links) starting with "Index:English/"
            // They are subcategories of 
            mainPageResponse.FilterLinks("Index:" + Language  + "/.*");

            // Obtain needed information
            // Expected data are: language | word | pronounciation (IPA format) | sound file

            // Iterate through letter indices
            foreach (WiktionaryResponseLink letterLink in mainPageResponse.Parse.Links)
            {
                // Parse specific letter index page
                WiktionaryResponse letterPageResponse = parser.Parse(letterLink.PageName);
                letterPageResponse.FilterLinks(0);

                // Iterate through entry links
                foreach (WiktionaryResponseLink entryLink in letterPageResponse.Parse.Links)
                {
                    // Parse specific entry page
                    WiktionaryResponse entryPageResponse = parser.Parse(entryLink.PageName);

                    // If page with entry does not exist, ignore it (unexpected error)
                    if (entryPageResponse == null)
                    {
                        continue;
                    }

                    // If entry does not contain any data (plain link to page, where entry can be defined by user)
                    // save it (only language + word)
                    if (entryPageResponse.Parse == null)
                    {
                        Word = entryLink.PageName;
                        Console.WriteLine(Word + "\t\t\tPlain link to page");
                        continue;
                    }

                    // If entry does not contain any sections, ignore it, as we cannot tell, whether word should be added to current language
                    // This is a filter mechanism for "*" links next to proper words
                    if (entryPageResponse.Parse.Sections == null)
                    {
                        continue;
                    }

                    // If sections exist, check whether it contains definition for our specific language
                    WiktionaryResponseSection section = entryPageResponse.Parse.Sections.Find(s => s.Name == Language);
                    
                    // If yes, check whether page contains IPA notation and/or sound file
                    // If no, ignore
                    if (section != null)
                    {
                        Word = entryLink.PageName;

                        WiktionaryResponseSection pronunciationSection = entryPageResponse.Parse.Sections.Find(s => s.Name == "Pronunciation" && s.Index > section.Index);

                        if (pronunciationSection != null)
                        {
                            WiktionaryResponse pronounciationSectionResponse = parser.Parse(entryLink.PageName, pronunciationSection.Index);
                            // Get IPA element
                            Regex ipaSpanRegex = new Regex("<span.*class=\"IPA\".*>(.*)</span>");
                            Match ipaMatch = ipaSpanRegex.Match(pronounciationSectionResponse.Parse.Text.Text);
                            IPANotation = ipaMatch.Groups[1].Value;
                            
                            // Test for link in pronounciation section. If it exists there, point a link
                            if (IPANotation != "")
                            {
                                Console.WriteLine(Word + " (IPA: " + IPANotation + ")");
                            }
                            else
                            {
                                Console.WriteLine(Word + "\t\t\t'Pronunciation' section exists. No IPA");
                            }

                            // Get sound file (images parse section)
                            // If sound file occurs in 'images' section with .OGG extention, use it
                            if(pronounciationSectionResponse.Parse.Images != null)
                            {
                                string soundFileName = pronounciationSectionResponse.Parse.Images.Find(img => img.Contains(".ogg") || img.Contains(".OGG"));
                                Regex soundFileUrlRegex = new Regex("src=\"(.*" + soundFileName + ")\"");
                                Match soundFileUrlMatch = soundFileUrlRegex.Match(pronounciationSectionResponse.Parse.Text.Text);
                                if (soundFileUrlMatch.Success)
                                {
                                    parser.DownloadFile("http:" + soundFileUrlMatch.Groups[1].Value, soundFileName);
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine(Word + "\t\t\tNo 'Pronunciation' section");
                        }
                    }
                }
            }
        }
    }
}
