﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace WikiSearch
{
    class WiktionaryResponse
    {
        [JsonProperty(PropertyName = "parse")]
        public WiktionaryResponseParse Parse { get; set; }

        // Remove links that do not match the pattern
        public void FilterLinks(string pattern)
        {
            Regex regex = new Regex(pattern);

            Parse.Links.RemoveAll(link =>
            {
                Match match = regex.Match(link.PageName);
                return !match.Success;
            });
        }

        // Remove links that do not match the namespace ID
        public void FilterLinks(int namespaceId)
        {
            Parse.Links.RemoveAll(link => link.Namespace != namespaceId);
        }
    }

    class WiktionaryResponseSection
    {
        [JsonProperty(PropertyName = "line")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "index")]
        public int Index { get; set; }
    }

    class WiktionaryResponseLink
    {
        [JsonProperty(PropertyName = "ns")]
        public int Namespace { get; set; }

        [JsonProperty(PropertyName = "*")]
        public string PageName { get; set; }
    }

    class WiktionaryResponseText
    {
        [JsonProperty(PropertyName = "*")]
        public string Text { get; set; }
    }

    class WiktionaryResponseParse
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "text")]
        public WiktionaryResponseText Text { get; set; }

        [JsonProperty(PropertyName = "links")]
        public List<WiktionaryResponseLink> Links { get; set; }

        [JsonProperty(PropertyName = "sections")]
        public List<WiktionaryResponseSection> Sections { get; set; }

        [JsonProperty(PropertyName = "images")]
        public List<string> Images { get; set; }
    }
}
